'use strict';


const path = require('path');
const Express = require('express');
const helmet = require('helmet');


const port = process.env.PORT || 8080;


const app = new Express();
app.use(helmet())
app.use(Express.static(path.join(__dirname, '..', 'public')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'index.html'))
});


app.listen(port);
console.log(`Server started on port ${port}`);

'use strict';

var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');

module.exports = {

  context: path.join(__dirname, '..', 'src'),

  entry: './index',

  resolve: {
      moduleDirectories: ['node_modules'],
      extensions: ['', '.js', '.jsx', '.scss']
  },

  output: {
    path: path.join(__dirname, '..', 'public'),
    filename: 'bundle.js',
  },

  module: {
      loaders: [
          {
              test: /\.jsx?$/,
              exclude: /node_modules/,
              loader: 'babel',
              query: {
                  extends: path.join(__dirname, '..', '.babelrc')
              }
          },
          {
              test: /\.scss$/,
              loader: ExtractTextPlugin.extract('css!sass!postcss')
          }
      ]
  },

  plugins: [
    new ExtractTextPlugin('style.css'),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack.optimize.UglifyJsPlugin({
        compressor: { warnings: false }
    })
  ],

  postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ]
};

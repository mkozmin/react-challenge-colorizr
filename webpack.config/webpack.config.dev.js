'use strict';

const path = require('path');
const webpack = require('webpack');


module.exports = {

    context: path.join(__dirname, '..', 'src'),

    entry: [
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './index'
    ],

    resolve: {
        moduleDirectories: ['node_modules'],
        extensions: ['', '.js', '.jsx', '.scss']
  },

    output: {
        path: path.join(__dirname, '..', 'public'),
        filename: 'bundle.js',
    },

    devtool: 'cheap-inline-module-source-map',

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                 loaders: [
                     'react-hot',
                     `babel?extends=${path.join(__dirname, '..', '.babelrc')}&cacheDirectory`
                 ]
            },
            {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass']
            }
        ]
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
};

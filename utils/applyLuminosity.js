import convert from 'color-convert';


export default function applyLuminosity(color) {
    const [ h, s, l ] = convert.hex.hsl(color);

    let darkest = l % 10;
    let colors = [];
    const hslToHex = convert.hsl.hex;

    // `Brightening` the darkest color by increasing its luminosity by 10%
    while (darkest < 100) {
        colors.push('#' + hslToHex([h, s, darkest]).toLowerCase());
        darkest += 10;
    }

    return colors;
}

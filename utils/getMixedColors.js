import convert from 'color-convert';


export default function getMixedColors(color, mixColor) {
    const hexToRgb = convert.hex.rgb;
    const rgbToHex = convert.rgb.hex;
    color = hexToRgb(color);
    mixColor = hexToRgb(mixColor);

    const colors = [];
    for (let i = 1; i <= 10; i++) {
        colors.push(
            '#' + rgbToHex(mixColors(color, mixColor, i / 10)).toLowerCase()
        )
    }

    return colors;
}


function mixColors(color1, color2, t) {
    const newColor = [];
    newColor[0] = mixColorValue(color1[0], color2[0], t);
    newColor[1] = mixColorValue(color1[1], color2[1], t);
    newColor[2] = mixColorValue(color1[2], color2[2], t);

    return newColor;
}


function mixColorValue(a, b, t) {
    return Math.sqrt(
        (1 - t) * Math.pow(a, 2) + t * Math.pow(b, 2)
    );
}

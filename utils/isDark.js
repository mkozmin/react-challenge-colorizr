import convert from 'color-convert';


export default function isDark(color) {
    return convert.hex.hsl(color)[2] < 50 ? true : false;
}

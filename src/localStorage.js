import { MAX_SELECTED_COLORS } from './constants';


export function loadSelectedColors() {
    try {
        const serialized = localStorage.getItem('selectedColors');
        if (serialized === null) {
            return undefined;
        }

        const selectedColors = JSON.parse(serialized);
        if(!validateSelectedColors(selectedColors)) {
            return undefined
        }

        return selectedColors;
    } catch(e) {
        console.error(e);
        return undefined;
    }
}


export function saveSelectedColors(selectedColors) {
    try {
        const serialized = JSON.stringify(selectedColors);
        localStorage.setItem('selectedColors', serialized);
    } catch (e) {
        console.error(e);
    }
}


export function validateColor(color) {
    return /^#[0-9a-f]{3}([0-9a-f]{3})?$/i.test(color) ?
        true :
        false;
}

export function validateSelectedColors(selectedColors) {
    if (!Array.isArray(selectedColors)) {
        return false;
    }

    if (selectedColors.length > MAX_SELECTED_COLORS) {
        return false;
    }

    if (!selectedColors.every(c => validateColor(c))) {
        return false;
    }

    return true;
}

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './palette.scss';

import Button from '../Button';
import PaletteColor from '../PaletteColor';
import {
    bulkDeleteColors,
    bulkSelectColors,
    selectColor,
    deleteColor
} from '../../actions/colorActions';


class Palette extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            darkBackground: false
        };

        this.renderColors = this.renderColors.bind(this);
        this.renderButtons = this.renderButtons.bind(this);
        this.handleBackgroundChange = this.handleBackgroundChange.bind(this);
    }

    handleBackgroundChange() {
        this.setState({
            darkBackground: !this.state.darkBackground
        });
    }

    renderButtons() {
        const { selectedColors, colors, bulkDeleteColors, bulkSelectColors } = this.props;

        const intersection = selectedColors.intersect(colors);

        const RemoveAllButton = intersection.size ?
            <Button
                text="Remove All"
                type={Button.TYPES.RED}
                onClick={bulkDeleteColors.bind(null, intersection)}
            />
            : null;

        const SelectAllButton = (
            <Button text="Select All" onClick={bulkSelectColors.bind(null, colors)} />
        );

        return (
            <div className="palette__buttons">
                <Button
                    text={`${this.state.darkBackground ? "Lighter" : "Darker"} background`}
                    onClick={this.handleBackgroundChange}
                />
                {SelectAllButton}
                {RemoveAllButton}
            </div>
        );
    }

    renderColors() {
        let { colors, selectedColors, selectColor, deleteColor } = this.props;

        colors = colors.map((c, i) => {
            const isSelected = selectedColors.includes(c.toLowerCase());
            const handleClick = isSelected ? deleteColor.bind(null, c) : selectColor.bind(null, c);
            return (
                <PaletteColor
                    key={i}
                    color={c}
                    isSelected={isSelected}
                    onClick={handleClick}
                />
            );
        });

        return colors;
    }

    render() {
        const darkBackground = this.state.darkBackground;
        const { selectedColors, colors } = this.props;

        return (
            <div className="palette">
                <div className={"palette__colors-container" +
                    (darkBackground ? ' palette__colors-container_dark' : '')}
                >
                    {this.renderColors()}
                </div>
                {this.renderButtons()}
            </div>
        );
    }
}

Palette.propTypes = {
    colors: PropTypes.oneOfType([
        PropTypes.instanceOf(Immutable.OrderedSet),
        PropTypes.instanceOf(Immutable.List)
    ]),
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
};


export default connect(null, {
    selectColor,
    deleteColor,
    bulkDeleteColors,
    bulkSelectColors
}
)(Palette);

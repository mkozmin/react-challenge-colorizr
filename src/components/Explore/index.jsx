import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './explore.scss';

import Selected from '../Selected';
import ColorScheme from '../ColorScheme';
import Spinner from '../Spinner';
import { fetchSchemesIfNeeded } from '../../actions/exploreActions';


class Explore extends React.Component {

    constructor(props) {
        super(props);

        this.renderColorSchemes = this.renderColorSchemes.bind(this);
    }

    componentDidMount() {
        this.props.fetchSchemesIfNeeded()
    }

    renderColorSchemes () {
        const { schemes, selectedColors } = this.props;

        return schemes.map((s, i) => (
            <ColorScheme
                key={i}
                name={s.get('name')}
                colors={s.get('colors')}
                selectedColors={selectedColors}
            />
        ));
    }

    render() {
        const { selectedColors, isFetching } = this.props;
        return (
            <div className="explore">
                <h1 className="explore__title">Explore predefined color schemes</h1>
                <Selected selectedColors={selectedColors} />
                {isFetching ? <Spinner /> : null}
                {this.renderColorSchemes()}
            </div>
        )
    }
}

Explore.propTypes = {
    schemes: PropTypes.instanceOf(Immutable.List),
    isFetching: PropTypes.bool,
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet)
};

Explore.defaultProps = {
    schemes: Immutable.List()
};


function mapStateToProps(state) {
    return {
        schemes: state.explore.get('schemes'),
        isFetching: state.explore.get('isFetching'),
        selectedColors: state.colors.get('selectedColors')
    };
}

export default connect(mapStateToProps, { fetchSchemesIfNeeded })(Explore);

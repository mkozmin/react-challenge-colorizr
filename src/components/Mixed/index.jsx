import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { ChromePicker } from 'react-color';

import './mixed.scss';

import Palette from '../Palette';


export default function Mixed(props) {
    const {
        selectedColors,
        mixColor,
        mixedColors,
        onMixColorChange,
        onPickerStateChange,
        showPicker
    } = props;

    return (
        <section className="mixed">
            <h1 className="mixed__header">Mixed With</h1>
            <div className="mixed__mixer"
                onClick={onPickerStateChange}
                style={{ backgroundColor: mixColor }}
            />
            <div
                className={"mixed__color-picker" + (showPicker ?
                    " mixed__color-picker_active" : "")}
                >
                <ChromePicker
                    color={mixColor}
                    onChangeComplete={onMixColorChange}
                    disableAlpha
                />
            </div>
            <Palette colors={mixedColors} selectedColors={selectedColors} />
        </section>
    );
}

Mixed.propTypes = {
    mixColor: PropTypes.string,
    mixedColors: PropTypes.instanceOf(Immutable.List),
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
    onMixColorChange: PropTypes.func,
    onPickerStateChange: PropTypes.func,
    showPicker: PropTypes.bool
};

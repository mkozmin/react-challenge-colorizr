import React, { PropTypes } from 'react';
import Immutable from 'immutable';

import { connect } from 'react-redux';
import { ChromePicker } from 'react-color';

import './create.scss';

import { changeColor} from '../../actions/colorActions';
import { toggleMixColorPicker, changeMixColor  } from '../../actions/mixedActions';
import Selected from '../Selected';
import DarkerLighter from '../DarkerLighter';
import Mixed from '../Mixed';
import isDark from '../../../utils/isDark';


function Create(props) {
    const {
        currentColor,
        selectedColors,
        darkenedColors,
        mixedColors,
        mixColor,
        showPicker,
        handleColorChange,
        handleMixColorChange,
        handlePickerDisplay
    } = props;
    const backgroundImage = `linear-gradient(to bottom, ${currentColor}, #222222)`;

    return (
        <div className="create" style={{ backgroundImage }}>
            <h1
                className="create__title"
                style={{ color: isDark(currentColor) ? '#fff': '#222' }}
            >Choose your color
            </h1>
            <div className="create__color-picker">
                <ChromePicker
                    color={currentColor}
                    onChangeComplete={handleColorChange}
                    disableAlpha />
            </div>
            <Selected selectedColors={selectedColors} />
            <div className="create__panel">
                <DarkerLighter
                    selectedColors={selectedColors}
                    darkenedColors={darkenedColors}
                />
            </div>
            <div className="create__panel">
                <Mixed
                    selectedColors={selectedColors}
                    mixedColors={mixedColors}
                    mixColor={mixColor}
                    showPicker={showPicker}
                    onMixColorChange={handleMixColorChange}
                    onPickerStateChange={handlePickerDisplay}
                />
            </div>
        </div>
    );
}

Create.propTypes = {
    currentColor: PropTypes.string.isRequired,
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
    darkenedColors: PropTypes.instanceOf(Immutable.List),
    mixColor: PropTypes.string,
    mixedColors: PropTypes.instanceOf(Immutable.List),
};


function mapStateToProps(state) {
    return {
        currentColor: state.colors.get('currentColor'),
        selectedColors: state.colors.get('selectedColors'),
        darkenedColors: state.darkerLighter.get('darkenedColors'),
        mixColor: state.mixed.get('mixColor'),
        mixedColors: state.mixed.get('mixedColors'),
        showPicker: state.mixed.get('showPicker')
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleColorChange: newColor => dispatch(changeColor(newColor.hex)),
        handleMixColorChange: newColor => dispatch(changeMixColor(newColor.hex)),
        handlePickerDisplay: () => dispatch(toggleMixColorPicker()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Create);

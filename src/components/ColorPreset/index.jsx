import React, { PropTypes } from 'react';
import Immutable from 'immutable';

import './color-preset.scss';


export default function ColorPreset(props) {
    let { colors, name, selectedColors, isSelected, onClick } = props;

    colors = colors.map((c, i) => (
        <div
            key={i}
            className="color-preset__color"
            style={{ backgroundColor: c }}
        />
    ));

    const icon = isSelected ?
        <i className="color-preset__icon fa fa-check" aria-hidden="true"/>
        : null;

    return (
        <div
            className={"color-preset" + (isSelected ? " color-preset_selected" : '')}
            onClick={onClick}
        >
            <h2 className="color-preset__name">{name}
                {icon}
            </h2>
            <div className="color-preset__colors">
                {colors}
            </div>
        </div>
    );
}

ColorPreset.propTypes = {
    colors: PropTypes.instanceOf(Immutable.List),
    name: PropTypes.string,
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
    isSelected: PropTypes.bool,
    onClick: PropTypes.func
};

import React, { PropTypes } from 'react';
import { IndexLink, Link } from 'react-router';

import './header.scss';


export default function Header(props) {
    return (
        <header className="header">
            <div className="header__content">
                <IndexLink to="/" className="header__logo-link">
                    <img role="presentation" className="header__logo" src="images/logo-dark.svg"></img>
                </IndexLink>
                <nav className="header__navigation">
                    <IndexLink
                        to="/"
                        className="header__link"
                        activeClassName="header__link_active">
                        Create
                    </IndexLink>
                    <Link
                        to="/explore"
                        className="header__link"
                        activeClassName="header__link_active">
                        Explore
                    </Link>
                    <Link
                        to="/presets"
                        className="header__link"
                        activeClassName="header__link_active">
                        Presets
                    </Link>
                    <Link
                        to="/export"
                        className="header__link"
                        activeClassName="header__link_active">
                        Export
                    </Link>
                </nav>
            </div>
        </header>
    );
}

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './color-scheme.scss';

import { selectColor, deleteColor } from '../../actions/colorActions';


function ColorScheme({ colors, name, selectedColors, selectColor, deleteColor }) {
    colors = colors.map((c, i) => {
        const isSelected = selectedColors.includes(c);
        const handleColorClick = isSelected ? deleteColor.bind(null, c) : selectColor.bind(null, c);
        return (
            <div
                className={"color-scheme__color-wrapper" + (isSelected ?
                    " color-scheme__color-wrapper_selected" : "")}
                key={i}
                onClick={handleColorClick}
            >
                <div
                    className={"color-scheme__color" + (isSelected ?
                        " color-scheme__color_selected" : "")}
                    style={{ backgroundColor: c }}
                />
                <span className="color-scheme__color-code">{c}</span>
            </div>
        );
    })

    return (
        <div className="color-scheme">
            <h2 className="color-scheme__name">{name}</h2>
            <div className="color-scheme__colors-container">
                {colors}
            </div>
        </div>
    );
}

ColorScheme.propTypes = {
    colors: PropTypes.instanceOf(Immutable.List),
    name: PropTypes.string,
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet)
};


export default connect(null, { selectColor, deleteColor })(ColorScheme);

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import './selected-color.scss';

import isDark from '../../../utils/isDark';


export default function SelectedColor({ color, onClick }) {
    return (
        <div className="selected-color">
            <div
                className={"selected-color__color" + (!color ?
                " selected-color__color_empty" : "")}
                style={{ backgroundColor: color ? color : '#cfcccc' }}
                onClick={onClick}
            >
            <i className="selected-color__icon selected-color__icon_remove fa fa-times"
                style={{
                    color: color ? isDark(color) ? '#fff' : '#222' : null,
                    opacity: color ? 1 : 0
                }}
                aria-hidden="true"
            />
            </div>
        </div>
    );
}

SelectedColor.propTypes = {
    color: PropTypes.string,
    onClick: PropTypes.func
};

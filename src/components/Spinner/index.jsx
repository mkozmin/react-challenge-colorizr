import React, { PropTypes } from 'react';

import './spinner.scss';


export default function Spinner() {
    return (<i className={"spinner fa fa-spinner fa-spin fa-3x fa-fw"} />);
};

import React, { PropTypes } from 'react';
import Immutable from 'immutable';

import './export-code.scss';

import { EXPORT_FORMATS } from '../../constants';
import ExportCodeLine from './ExportCodeLine';
import Button from '../Button';


export default function ExportCode(props) {
    const { variables, selectedColors, format, onExportFormatChange }
        = props;

    if (selectedColors.size === 0) {
        return null;
    }

    // c[0] is for variable name, c[1] is for hex
    const lines = variables.zip(selectedColors).map((c, i) => {
        return (
            <ExportCodeLine key={i} variable={c[0]} color={c[1]} format={format} />
        );
    });
    return (
        <div className="export-code">
            <h2 className="export-code__header">Export your code</h2>
            <div className="export-code__buttons">
                <Button
                    text="Sass"
                    onClick={onExportFormatChange.bind(null, EXPORT_FORMATS.SASS)}
                    type={format === EXPORT_FORMATS.SASS ? Button.TYPES.PINK : null}
                    disabled={format === EXPORT_FORMATS.SASS ? true : false}
                />
                <Button
                    text="Less"
                    onClick={onExportFormatChange.bind(null, EXPORT_FORMATS.LESS)}
                    type={format === EXPORT_FORMATS.LESS ? Button.TYPES.PINK : null}
                    disabled={format === EXPORT_FORMATS.LESS ? true : false}
                />
                <Button
                    text="Stylus"
                    onClick={onExportFormatChange.bind(null, EXPORT_FORMATS.STYLUS)}
                    type={format === EXPORT_FORMATS.STYLUS ? Button.TYPES.PINK : null}
                    disabled={format === EXPORT_FORMATS.STYLUS ? true : false}
                />
            </div>
            <pre className="export-code__code-wrapper">
                <code className="export-code__code">{lines}</code>
            </pre>
        </div>
    );
}

ExportCode.propTypes = {
    variables: PropTypes.instanceOf(Immutable.List),
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
    format: PropTypes.string,
    onExportFormatChange: PropTypes.func,
};

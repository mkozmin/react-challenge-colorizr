import React, { PropTypes } from 'react';

import { EXPORT_FORMATS } from '../../constants';


export default function ExportCodeLine({ variable, color, format }) {

    switch (format) {

        case EXPORT_FORMATS.SASS:
            return (
                <div className="export-code__line">
                    <span className="export-code__var">${variable}</span>:&nbsp;
                    {color};
                </div>
            );

        case EXPORT_FORMATS.LESS:
            return (
                <div className="export-code__line">
                    <span className="export-code__var">@{variable}</span>:&nbsp;
                    {color};
                </div>
            );

        case EXPORT_FORMATS.STYLUS:
            return (
                <div className="export-code__line">
                    <span className="export-code__var">{variable}</span>
                    &nbsp;=&nbsp;{color}
                </div>
            );

        default:
            return null;
    }
}

ExportCodeLine.propTypes = {
    variable: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
};

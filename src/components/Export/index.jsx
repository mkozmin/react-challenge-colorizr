import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './export.scss';

import SelectedTable from '../SelectedTable';
import ExportCode from '../ExportCode';
import { changeVariableName, changeExportFormat } from '../../actions/exportActions';


class Export extends React.Component {

    constructor(props) {
        super(props);

        this.handleVariableNameChange = this.handleVariableNameChange.bind(this);
    }

    handleVariableNameChange(e) {
        const { changeVariableName } = this.props;
        const target = e.target;

        const index = target.dataset.index;
        changeVariableName(index, target.value);
    }

    render() {
        const { selectedColors, variables, format, changeExportFormat }
            = this.props;
        return (
            <div className="export">
                <h1 className="export__title">
                    Customize and export colors for Sass, Less or Stylus
                </h1>
                <SelectedTable
                    selectedColors={selectedColors}
                    variables={variables}
                    onVariableNameChange={this.handleVariableNameChange}
                />
                <ExportCode
                    selectedColors={selectedColors}
                    variables={variables}
                    format={format}
                    onExportFormatChange={changeExportFormat}
                />
            </div>
        );
    }
};


Export.propTypes = {
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
    variables: PropTypes.instanceOf(Immutable.List),
    format: PropTypes.string
};


function mapStateToProps(state) {
    return {
        selectedColors: state.colors.get('selectedColors'),
        variables: state.export.get('variables'),
        format: state.export.get('format')
    };
}


export default connect(
    mapStateToProps,
    { changeVariableName, changeExportFormat }
)(Export);

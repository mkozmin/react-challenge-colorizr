import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './presets.scss';

import { fetchPresetsIfNeeded } from '../../actions/presetsActions';
import { replaceColors } from '../../actions/colorActions';
import ColorPreset from '../ColorPreset';
import Selected from '../Selected';
import Spinner from '../Spinner';


class Presets extends React.Component {

    constructor(props) {
        super(props);

        this.renderColorPresets = this.renderColorPresets.bind(this);
    }

    componentDidMount() {
        this.props.fetchPresetsIfNeeded();
    }

    renderColorPresets() {
        const { selectedColors, presets, replaceColors } = this.props;
        
        return presets.map((p, i) => {
            const colors = p.get('colors');
            const name = p.get('name');

            const isSelected = colors.isSubset(selectedColors);
            const handleClick = isSelected ? null : replaceColors.bind(null, colors);

            return (
                <ColorPreset
                    key={i}
                    name={name}
                    colors={colors}
                    selectedColors={selectedColors}
                    isSelected={isSelected}
                    onClick={handleClick}
                />
            );
        });
    }

    render() {
        const { selectedColors, isFetching } = this.props;

        return (
            <div className="presets">
                <h1 className="presets__title">... or use these presets</h1>
                <Selected selectedColors={selectedColors} />
                {isFetching ? <Spinner /> : null}
                {this.renderColorPresets()}
            </div>
        );
    }
}

Presets.propTypes = {
    presets: PropTypes.instanceOf(Immutable.List),
    isFetching: PropTypes.bool,
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet)
};

Presets.defaultProps = {
    presets: Immutable.List()
};


function mapStateToProps(state) {
    return {
        presets: state.presets.get('presets'),
        isFetching: state.presets.get('isFetching'),
        selectedColors: state.colors.get('selectedColors')
    };
}


export default connect(
    mapStateToProps,
    { fetchPresetsIfNeeded, replaceColors }
)(Presets);

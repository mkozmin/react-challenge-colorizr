import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import './palette-color.scss';

import isDark from '../../../utils/isDark';


export default function PaletteColor({ color, isSelected=false, onClick }) {

    const colorIcon = (
        <i className={"fa fa-plus palette-color__icon" +
            (isSelected ? " palette-color__icon_remove" : " palette-color__icon_add")}
            style={{ color: isDark(color) ? '#fff' : '#222' }}
            aria-hidden="true"
        />);

    return (
        <div
            className={'palette-color' + (isSelected ? ' palette-color_selected' : '')}
            style={{ backgroundColor: color }}
            onClick={onClick}
        >
        {colorIcon}
        </div>
    );
}

PaletteColor.propTypes = {
    color: PropTypes.string,
    isSelected: PropTypes.bool,
    onClick: PropTypes.func
};

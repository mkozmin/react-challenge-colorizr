import React, { PropTypes } from 'react';

import './button.scss';


export default function Button({ text, type, onClick, disabled=false }) {
    return (
        <button
            className={type ? `button ${type}` : 'button'}
            onClick={onClick}
            disabled={disabled}
            >
            {text}
        </button>
    );
}

Button.TYPES = {
    PINK: 'button_pink',
    RED: 'button_red'
};

Button.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool
};

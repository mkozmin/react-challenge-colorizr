import React from 'react';

import '../styles/main.scss';

import Header from './Header';
import Footer from './Footer';
import GithubLink from './GithubLink';


export default function App({ children }) {
    return (
        <div className="app">
            <GithubLink
                url="https://github.com/mkozmin/react-challenge-colorizr"
            />
            <Header />
            {children}
            <Footer />
        </div>
    );

}

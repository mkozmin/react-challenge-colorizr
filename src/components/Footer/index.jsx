import React, { PropTypes } from 'react';

import './footer.scss';


export default function Footer(props) {
    return (
        <footer className="footer">
            <img className="footer__logo" src="images/logo-light.svg"></img>
        </footer>
    );
}

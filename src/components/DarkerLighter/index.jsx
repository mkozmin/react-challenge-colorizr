import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import Immutable from 'immutable';
import convert from 'color-convert';

import './darker-lighter.scss';

import Palette from '../Palette';


export default function DarkerLighter({ darkenedColors, selectedColors }) {
    return (
        <section className="darker-lighter">
            <h1 className="darker-lighter__header">Darker and Lighter</h1>
            <Palette
                colors={darkenedColors}
                selectedColors={selectedColors}
            />
        </section>
    );
}

DarkerLighter.propTypes = {
    darkenedColors: PropTypes.instanceOf(Immutable.List),
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet)
};

import React, { PropTypes } from 'react';
import convert from 'color-convert';
import Immutable from 'immutable';

import './selected-table.scss';


export default function SelectedTable(props) {
    const { selectedColors, variables, onVariableNameChange } = props;
    return (
        <table className="selected-table">
            <tbody>
                <tr className="selected-table__header">
                    <th className="selected-table__cell">Color</th>
                    <th className="selected-table__cell selected-table__cell_hex">Hex value</th>
                    <th className="selected-table__cell selected-table__cell_rgb">RGB value</th>
                    <th className="selected-table__cell">Variable name</th>
                </tr>
                {renderRows(selectedColors, variables, onVariableNameChange)}
            </tbody>
        </table>
    );
}


SelectedTable.propTypes = {
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
    variables: PropTypes.instanceOf(Immutable.List),
    onVariableNameChange: PropTypes.func
};


function renderRows(selectedColors, variables, onVariableNameChange) {
    const hexToRgb = convert.hex.rgb;

    if (selectedColors.size === 0) {
        return (
            <tr className="selected-table__row">
                <td className="selected-table__cell"
                    colSpan="4">
                    Select colors before exporting.
                </td>
            </tr>
        );
    }

    // c[0] is for variable name, c[1] is for hex
    return variables.zip(selectedColors).map((c, i) => {
        const rgb = hexToRgb(c);
        return (
            <tr className="selected-table__row" key={i}>
                <td
                    className="selected-table__cell selected-table__cell_color"
                    style={{ backgroundColor: c[1] }}
                />
                <td
                    className="selected-table__cell selected-table__cell_hex">
                    {c[1]}
                </td>
                <td
                    className="selected-table__cell selected-table__cell_rgb">
                    {`rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`}
                </td>
                <td
                    className="selected-table__cell selected-table__cell_var">
                    <input
                        className="selected-table__input"
                        type="text"
                        data-index={i}
                        name="variable"
                        onChange={onVariableNameChange}
                        value={c[0]}
                    />
                </td>
            </tr>
        );
    });
}

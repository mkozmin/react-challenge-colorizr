import React, { PropTypes } from 'react';

import './github-link.scss';


export default function GithubLink({ url }) {
    return (
        <a
            className="github-link"
            href={url} >
            <img
                className="github-link__image"
                src="images/GitHub-Mark-64px.png"
                alt="github"
                width="64"
                height="64"
            />
        </a>
    );
};


GithubLink.propTypes = {
    url: PropTypes.string
};

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './selected.scss';

import { MAX_SELECTED_COLORS } from '../../constants';
import SelectedColor from '../SelectedColor';
import { deleteColor } from '../../actions/colorActions';


class Selected extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fixed: false
        };

        this.placeholderInitialHeight = 0;
        this.breakpointOffset = 0;

        this.renderColors = this.renderColors.bind(this);
        this.calcPlaceholderInitialHeight = this.calcPlaceholderInitialHeight.bind(this);
        this.calcBreakpointOffset = this.calcBreakpointOffset.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        if (screen.width >= 768) {
            this.placeholderInitialHeight = this.calcPlaceholderInitialHeight();
            this.breakpointOffset = this.calcBreakpointOffset();
            window.addEventListener('scroll', this.handleScroll);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
            const { top } = this._placeholderNode.getBoundingClientRect();
            const needToFix = top <= this.breakpointOffset && !this.state.fixed;
            const needToRelease = top > this.breakpointOffset && this.state.fixed;

            if (needToFix) {
                this.setState({ fixed: true });
                return;
            }

            if (needToRelease) {
                this.setState({ fixed: false });
                return;
            }
    }

    calcPlaceholderInitialHeight() {
        return this._placeholderNode.offsetHeight;
    }

    calcBreakpointOffset() {
        return -this._placeholderNode.offsetHeight;
    }

    renderColors() {
        const { selectedColors, deleteColor } = this.props;
        const colors = [];

        // The key is necessary since there is no index
        // available within OrderedSet.forEach callback.
        let key = 0;
        selectedColors.forEach(c => {
            colors.push(
                <SelectedColor
                    key={key++}
                    color={c}
                    onClick={deleteColor.bind(null, c)}
                />
            )
        });

        //.selected-color_empty
        for (key; key < MAX_SELECTED_COLORS; key++) {
            colors.push(<SelectedColor key={key} />);
        }
        return colors;
    }

    render() {
        const { fixed } = this.state;

        const placeholderStyle = fixed ?
            { height: this.placeholderInitialHeight }
            : null;

        const stickyClass = 'selected__sticky' + (fixed ? ' selected__sticky_fixed' : '');

        return (
            <section className="selected">
                <h1 className="selected__header">Selected colors:<br/>
                    <small>Select up to {MAX_SELECTED_COLORS} colors.</small>
                </h1>
                <div
                    className="selected__sticky-placeholder"
                    ref={placeholder => this._placeholderNode = placeholder}
                    style={placeholderStyle}
                >
                    <div
                        className={stickyClass}
                        ref={stickyNode => this._stickyNode = stickyNode}
                    >
                        <div
                            className="selected__colors"
                        >
                            {this.renderColors()}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

Selected.propTypes = {
    selectedColors: PropTypes.instanceOf(Immutable.OrderedSet),
};


export default connect(null, { deleteColor })(Selected);

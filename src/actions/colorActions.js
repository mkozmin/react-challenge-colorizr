import * as types from './actionTypes';


export function changeColor(color) {
    return {
        type: types.CHANGE_COLOR,
        currentColor: color
    };
}


export function selectColor(color) {
    return {
        type: types.SELECT_COLOR,
        color
    };
}


export function bulkSelectColors(colors) {
    return {
        type: types.BULK_SELECT_COLORS,
        colors
    };
}


export function deleteColor(color) {
    return {
        type: types.DELETE_COLOR,
        color
    };
}


export function bulkDeleteColors(colors) {
    return {
        type: types.BULK_DELETE_COLORS,
        colors
    };
}


export function replaceColors(colors) {
    return {
        type: types.REPLACE_COLORS,
        colors
    };
}

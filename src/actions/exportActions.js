import * as types from './actionTypes';



export function changeVariableName(index, name) {
    return {
        type: types.CHANGE_VARIABLE_NAME,
        index,
        name
    };
}


export function changeExportFormat(format) {
    return {
        type: types.CHANGE_EXPORT_FORMAT,
        format
    };
}

import * as types from './actionTypes';

import getMixedColors from '../../utils/getMixedColors';


export function changeMixColor(mixColor) {

    return (dispatch, getState) => {
        const currentColor = getState().colors.get('currentColor');
        const mixedColors = getMixedColors(currentColor, mixColor);

        return dispatch(
            {
                type: types.CHANGE_MIX_COLOR,
                mixColor,
                mixedColors
            }
        );
    };
}


export function toggleMixColorPicker() {
    return {
        type: types.TOGGLE_MIX_COLOR_PICKER,
    };
}

import * as types from './actionTypes';

import { CALL_FIREBASE } from '../middleware/firebaseMiddleware';


export function fetchSchemesIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchSchemes(getState())) {
            return dispatch(fetchSchemes());
        }

        return Promise.resolve();
    };
}

function fetchSchemes() {
    return dispatch => {
        const config = {
            endpoint: 'schemes',
            successType: types.FETCH_SCHEMES_SUCCESS,
            failureType: types.FETCH_SCHEMES_FAILURE
        };

        dispatch({
            type: types.FETCH_SCHEMES_REQUEST,
            [CALL_FIREBASE]: config
        });
    }
}

function shouldFetchSchemes(state) {
    return state.explore.get('schemes').size === 0 ? true : false;
}

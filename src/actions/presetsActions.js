import * as types from './actionTypes';
import { CALL_FIREBASE } from '../middleware/firebaseMiddleware';


export function fetchPresetsIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchPresets(getState())) {
            return dispatch(fetchPresets());
        }

        return Promise.resolve();
    };
}

function fetchPresets() {
    return dispatch => {
        const config = {
            endpoint: 'presets',
            successType: types.FETCH_PRESETS_SUCCESS,
            failureType: types.FETCH_PRESETS_FAILURE
        };

        dispatch({
            type: types.FETCH_PRESETS_REQUEST,
            [CALL_FIREBASE]: config
        });
    }
}

function shouldFetchPresets(state) {
    return state.presets.get('presets').size === 0 ? true : false;
}

import React from 'react';
import { Router, Route, IndexRoute, browserHistory} from 'react-router';

import App from './components/App';
import Create from './components/Create';
import Explore from './components/Explore';
import Presets from './components/Presets';
import Export from './components/Export';


const routes = (
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Create} />
            <Route path="explore" component={Explore} />
            <Route path="presets" component={Presets} />
            <Route path="export" component={Export} />
        </Route>
    </Router>
);

export default routes;

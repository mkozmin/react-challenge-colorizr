import Immutable from 'immutable';
import { MAX_SELECTED_COLORS, EXPORT_FORMATS } from '../constants';

import * as types from '../actions/actionTypes';


const variables = [];
for (let i = 1; i <= MAX_SELECTED_COLORS; i++) {
    variables.push(`color-${i}`);
}

const initialState = Immutable.Map({
    variables: Immutable.List(variables),
    format: EXPORT_FORMATS.SASS
});

export default function exportReducer(state=initialState, action) {

    switch (action.type) {

        case types.CHANGE_VARIABLE_NAME:
            return state.setIn(['variables', action.index], action.name);

        case types.CHANGE_EXPORT_FORMAT:
            return state.set('format', action.format);

        default:
            return state;
    }
}

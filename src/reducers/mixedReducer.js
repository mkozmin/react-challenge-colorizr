import Immutable from 'immutable';
import convert from 'color-convert';

import * as types from '../actions/actionTypes';
import getMixedColors from '../../utils/getMixedColors';

const initialState = Immutable.Map({
    mixColor: '#f2c812',
    mixedColors: Immutable.List(),
    showPicker: false
});


export default function mixedReducer(state=initialState, action) {

    switch (action.type) {

        case types.CHANGE_COLOR:
            return state.set(
                'mixedColors',
                Immutable.List(
                    getMixedColors(action.currentColor, state.get('mixColor'))
                )
            );

        case types.CHANGE_MIX_COLOR:
                return state.merge(
                    {
                        mixColor: action.mixColor,
                        mixedColors: action.mixedColors
                    }
                );

        case types.TOGGLE_MIX_COLOR_PICKER:
            return state.set('showPicker', !state.get('showPicker'));

        default:
            return state;
    }
}

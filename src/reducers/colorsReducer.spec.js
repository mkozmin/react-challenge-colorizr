import { expect } from 'chai';
import Immutable from 'immutable';

import colorsReducer, { initialState } from './colorsReducer';
import * as types from '../actions/actionTypes';
import { MAX_SELECTED_COLORS } from '../constants';


const testState = Immutable.Map({
    currentColor: '#dfddee',
    selectedColors: Immutable.OrderedSet([
        '#ee3fff',
        '#97d',
        '#35e710',
        '#2d50ba',
        '#f13',
        '#2f2f2f',
        '#edaf26',
        '#42100b',
        '#cdc82a',
        '#de162e'
    ])
});


describe('colorsReducer', () => {
    it('should return initialState', () => {
        expect(
            colorsReducer(undefined, {})
        ).to.equal(initialState);
    });

    it('should handle CHANGE_COLOR', () => {
        const newState = colorsReducer(
            testState,
            {
                type: types.CHANGE_COLOR,
                currentColor: '#ffffff'
            }
        );

        expect(newState.get('currentColor')).to.equal('#ffffff');
    });

    it('should handle SELECT_COLOR', () => {
        const newState = colorsReducer(
            testState,
            {
                type: types.SELECT_COLOR,
                color: '#ffffff'
            }
        )
        expect(newState.get('selectedColors').last())
            .to.equal('#ffffff');
    });

    it('should handle SELECT_COLOR if selectedColors is full', () => {
        const newState = colorsReducer(
            testState,
            {
                type: types.SELECT_COLOR,
                color: '#ffffff'
            }
        );

        // Last n - 1 colors in the old state to be the same as
        // first n - 1 colors in the new state if old state already
        // contains n colors. n is MAX_SELECTED_COLORS.
        expect(
            newState.get('selectedColors').butLast().equals(
                    testState.get('selectedColors').rest()
            )
        ).to.equal(true);
    });

    it('should handle DELETE_COLOR', () => {
        const colorToDelete = testState.get('selectedColors').last();
        const newState = colorsReducer(
            testState,
            {
                type: types.DELETE_COLOR,
                color: colorToDelete
            }
        );

        expect(newState.get('selectedColors').contains(colorToDelete))
            .to.equal(false);
    });

    it('should handle REPLACE_COLORS', () => {
        const colorsToReplace = ['#dde', '#aed111', '#dfd333'];
        const newState = colorsReducer(
            testState,
            {
                type: types.REPLACE_COLORS,
                colors: colorsToReplace
            }
        );

        expect(newState
                .get('selectedColors')
                .equals(Immutable.OrderedSet(colorsToReplace))
        ).to.equal(true);
    });

    it('should handle BULK_SELECT_COLORS', () => {
        const colorsToSelect = ['#fceeee', '#1ddeef', '#aee134'];
        const newState = colorsReducer(
            testState,
            {
                type: types.BULK_SELECT_COLORS,
                colors: colorsToSelect
            });

        expect(
            newState
                .get('selectedColors')
                .takeLast(colorsToSelect.length)
                .equals(Immutable.OrderedSet(colorsToSelect))
        ).to.equal(true);

    });

    it('should handle BULK_DELETE_COLORS', () => {
        const colorsToDelete = testState
            .get('selectedColors')
            .slice(4, 7);

        const newState = colorsReducer(
            testState,
            {
                type: types.BULK_DELETE_COLORS,
                colors: colorsToDelete.toJS()
            });

        expect(newState.get('selectedColors').contains(colorsToDelete))
            .to.equal(false)
    });
});

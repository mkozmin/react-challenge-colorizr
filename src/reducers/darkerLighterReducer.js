import Immutable from 'immutable';

import * as types from '../actions/actionTypes';
import applyLuminosity from '../../utils/applyLuminosity';


const initialState = Immutable.Map({
    darkenedColors: Immutable.List()
});


export default function darkerLighterReducer(state=initialState, action) {

    switch (action.type) {

        case types.CHANGE_COLOR:
            return state.set(
                'darkenedColors',
                Immutable.List(applyLuminosity(action.currentColor))
            );

        default:
            return state;

    }
}

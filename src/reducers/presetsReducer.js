import Immutable from 'immutable';

import * as types from '../actions/actionTypes';


const initialState = Immutable.Map({
    presets: Immutable.List(),
    isFetching: false
});


export default function presetsReducer(state=initialState, action) {

    switch (action.type) {

        case types.FETCH_PRESETS_REQUEST:
            return state.set('isFetching', action.isFetching);

        case types.FETCH_PRESETS_SUCCESS:
            return state.merge({
                presets: Immutable.fromJS(action.payload),
                isFetching: action.isFetching
            });

        case types.FETCH_PRESETS_FAILURE:
            return state.set('isFetching', action.isFetching);

        default:
            return state;
    }
}

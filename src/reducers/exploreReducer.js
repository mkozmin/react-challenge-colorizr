import Immutable from 'immutable';


import * as types from '../actions/actionTypes';


const initialState = Immutable.Map({
    schemes: Immutable.List(),
    isFetching: false
});


export default function exploreReducer(state=initialState, action) {

    switch (action.type) {

        case types.FETCH_SCHEMES_REQUEST:
            return state.set('isFetching', action.isFetching);

        case types.FETCH_SCHEMES_SUCCESS:
            return state.merge({
                schemes: Immutable.fromJS(action.payload),
                isFetching: action.isFetching
            });

        case types.FETCH_SCHEMES_FAILURE:
            return state.set('isFetching', action.isFetching);

        default:
            return state;
    }
}

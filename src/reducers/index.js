import { combineReducers } from 'redux';

import colorsReducer from './colorsReducer';
import mixedReducer from './mixedReducer';
import darkerLighterReducer from './darkerLighterReducer';
import exploreReducer from './exploreReducer';
import presetsReducer from './presetsReducer';
import exportReducer from './exportReducer';


export default combineReducers({
    colors: colorsReducer,
    darkerLighter: darkerLighterReducer,
    mixed: mixedReducer,
    explore: exploreReducer,
    presets: presetsReducer,
    export: exportReducer
});

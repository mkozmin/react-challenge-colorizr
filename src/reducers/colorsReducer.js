import Immutable from 'immutable';

import * as types from '../actions/actionTypes';
import { MAX_SELECTED_COLORS } from '../constants';


export const initialState = Immutable.Map({
    currentColor: '#4a4a86',
    selectedColors: Immutable.OrderedSet(),
});


export default function colorsReducer(state=initialState, action) {

    switch (action.type) {

        case types.CHANGE_COLOR:
            return state.set('currentColor', action.currentColor.toLowerCase());

        case types.SELECT_COLOR:
            return state.update('selectedColors', s => (
                s.size < MAX_SELECTED_COLORS ?
                    s.add(action.color.toLowerCase()) :
                    s.rest().add(action.color.toLowerCase())
            ));

        case types.DELETE_COLOR:
            return state.update('selectedColors', c => c.delete(action.color.toLowerCase()));

        case types.REPLACE_COLORS:
            return state.set('selectedColors', Immutable.OrderedSet(action.colors));

        case types.BULK_SELECT_COLORS:
            return state.update('selectedColors', s => (
                s.concat(action.colors).takeLast(MAX_SELECTED_COLORS)
            ));

        case types.BULK_DELETE_COLORS:
            return state.update('selectedColors', s => s.subtract(action.colors));

        default:
            return state;
    }
}

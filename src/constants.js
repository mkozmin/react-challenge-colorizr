export const MAX_SELECTED_COLORS = 10;

export const EXPORT_FORMATS = {
    SASS: 'SASS',
    LESS: 'LESS',
    STYLUS: 'STYLUS'
};

export const FIREBASE_CONFIG = {
    apiKey: 'AIzaSyBt4eLREnvGRh3y6qQOAy0RUEUcqR59MD0',
    databaseURL: 'https://project-5538758502158987601.firebaseio.com/',
};

import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';

import firebaseMiddleware from '../middleware/firebaseMiddleware';
import rootReducer from '../reducers';


const middleware = [
    thunkMiddleware,
    firebaseMiddleware,
];


export default function configureStore(initialState) {
    const store = createStore(rootReducer, initialState, compose(
        applyMiddleware(...middleware),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    ));
    return store;
}

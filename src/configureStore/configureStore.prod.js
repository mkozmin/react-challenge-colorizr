import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';

import firebaseMiddleware from '../middleware/firebaseMiddleware';
import rootReducer from '../reducers';


const middleware = [
    thunkMiddleware,
    firebaseMiddleware
];


export default function configureStore(initialState) {
    const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));
    return store;
}

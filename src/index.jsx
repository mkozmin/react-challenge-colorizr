import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
import Immutable from 'immutable';

import configureStore from './configureStore';
import routes from './routes';
import { changeColor } from './actions/colorActions';
import { saveSelectedColors, loadSelectedColors } from './localStorage';


if (process.env.NODE_ENV !== 'production') {
    window.Perf = require('react-addons-perf');
}


// retrieve initial state from localStorage on page reload
const selectedColors = Immutable.OrderedSet(loadSelectedColors());
const persistedState = { colors: Immutable.Map({ selectedColors }) };

const store = configureStore(persistedState);

store.dispatch(changeColor('#4a4a86'));


// save state to localStorage on every change
store.subscribe(() => saveSelectedColors(
    store.getState().colors.get('selectedColors')
));


render(
    <Provider store={store}>{routes}</Provider>,
    document.getElementById('app')
    );

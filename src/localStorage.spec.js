import { assert } from 'chai';

import { validateColor, validateSelectedColors } from './localStorage';


describe('validateColor', () => {
    it('returns true on valid hex color string', () => {
        assert.equal(validateColor('#ffffff'), true);
        assert.equal(validateColor('#35dfe3'), true);
        assert.equal(validateColor('#1f3eea'), true);
        assert.equal(validateColor('#ff8'), true);
        assert.equal(validateColor('#118'), true);
        assert.equal(validateColor('#f0e'), true);
    });

    it('returns false on invalid hex color string', () => {
        assert.equal(validateColor('#rfffff'), false);
        assert.equal(validateColor('#r4fjwf'), false);
        assert.equal(validateColor('#1tf'), false);
        assert.equal(validateColor('#efe13'), false);
        assert.equal(validateColor('e1f#e13'), false);
        assert.equal(validateColor('2#efe131'), false);
        assert.equal(validateColor('2efe13'), false);
        assert.equal(validateColor([]), false);
        assert.equal(validateColor({}), false);
        assert.equal(validateColor(undefined), false);
        assert.equal(validateColor(f => f), false);
        assert.equal(validateColor(432132), false);
    });
});

const testColors = [
    '#ee3fff',
    '#97d',
    '#35e710',
    '#2d50ba',
    '#f13',
    '#2f2f2f',
    '#edaf26',
    '#42100b',
    '#cdc82a',
    '#de162e'
];

describe('validateSelectedColors', () => {

    it('returns true on array of valid hex colors', () => {
        assert.equal(validateSelectedColors(testColors), true);
        assert.equal(validateSelectedColors(testColors.slice(3, 5)), true);
    });

    it('returns true on empty array', () => {
        assert.equal(validateSelectedColors([]), true);
    });

    it('returns false on array with invalid hex color(-s)', () => {
        assert.equal(validateSelectedColors([...testColors.slice(1, 6), '#fco']), false);
        assert.equal(validateSelectedColors([...testColors.slice(1, 6), '#d33d']), false);
        assert.equal(validateSelectedColors([...testColors.slice(1, 6), 'e#fdd11']), false);
        assert.equal(validateSelectedColors([...testColors.slice(1, 6), 'e#fdd11e']), false);
    });

    it('returns false on non-array input', () => {
        assert.equal(validateSelectedColors({}), false);
        assert.equal(validateSelectedColors(undefined), false);
        assert.equal(validateSelectedColors(f => f), false);
    });
});

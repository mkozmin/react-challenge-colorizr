import path from 'path';
import firebase from 'firebase';

import { FIREBASE_CONFIG } from '../constants';


firebase.initializeApp(FIREBASE_CONFIG);

const db = firebase.database();


export const CALL_FIREBASE = Symbol('firebase');


export default function firebaseMiddleware({ dispatch }) {
    return next => action => {
        const callFirebase = action[CALL_FIREBASE];

        if (typeof callFirebase === 'undefined') {
            return next(action);
        }

        next({ type: action.type, isFetching: true });

        const { endpoint, successType, failureType } = callFirebase;

        return db.ref(endpoint)
            .once("value")
            .then(snapshot => snapshot.val())
            .then(payload => dispatch({ type: successType, isFetching: false, payload }))
            .catch(error => {
                dispatch({ type: failureType, isFetching: false, error })
                console.error(error);
            });
    }
}
